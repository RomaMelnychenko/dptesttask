//
//  BaseInteractor.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 14.04.2021.
//

import Foundation
import Alamofire

class BaseInteractor<I, T> where T: Decodable {
    
    enum Endpoint: String {
        case companies = "/companies/1"
        case city = "/city/1"
        
        fileprivate static var basePath: String {
            return "https://stage.mister.am/public-api"
        }
        
        fileprivate var path: String {
            return Endpoint.basePath + rawValue
        }
    }
    
    var items: [I] = []
    
    func request(_ endpoint: Endpoint, _ completion: GenericClosure<Alamofire.Result<T>>?) {
        Alamofire.request(endpoint.path).responseDecodable { (response: DataResponse<T>) in
            completion?(response.result)
        }
    }
}
