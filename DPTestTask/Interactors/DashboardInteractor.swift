//
//  DashboardInteractor.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import Foundation
import Alamofire

class DashboardInteractor: BaseInteractor<CompanyProtocol, Restourants> {
    
    static let shared = DashboardInteractor()
    private override init() {}
    
    var companies: [CompanyProtocol] {
        if let filterClosure = filterClosure {
            return items.filter(filterClosure)
        }
        return items
    }
    
    fileprivate var filterClosure: FilterClosure?
}

extension DashboardInteractor: DashboardInteractorProtocol {
    
    func availableDeliveryCompaniesCount(by filter: FilterProtocol) -> UInt {
        var count = items.count
        if let id = filter.type {
            count = companies.filter({ $0.availableDeliveryTypes?.contains(id) ?? false }).count
        }
        return UInt(count)
    }
    
    func setupCompanies(filter: FilterClosure?) {
        filterClosure = filter
    }
    
    func loadRestaurants(_ completion: ErrorClosure?) {
        guard companies.isEmpty else {
            completion?(nil)
            return
        }
        request(.companies) { (result) in
            switch result {
            case .success(let value):
                self.items = value
                completion?(nil)
            case .failure(let error):
                completion?(error)
            }
        }
    }
}

extension WorkingTime: WorkingTimeProtocol {}
extension Restourant: CompanyProtocol {
    var schedule: [WorkingTimeProtocol]? {
        return workingTime
    }
    
    var saleCount: UInt? {
        return UInt(actionInformerText?.count ?? 0)
    }
}
