//
//  FilterInteractor.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 14.04.2021.
//

import Foundation

class FilterInteractor: BaseInteractor<FilterProtocol, City> {
    
    fileprivate var companyDataSource: CompaniesDataSource
    init(companyDataSource: CompaniesDataSource) {
        self.companyDataSource = companyDataSource
    }
    
    var filters: [FilterProtocol] {
        let firstItem = DeliveryDatum(type: nil, title: "Усі способи отримання")
        return [firstItem] + items
    }
}

extension FilterInteractor: FilterInteractorProtocol {
    
    func updateFilterCLosure(with selectedFilter: FilterProtocol) {
        companyDataSource.setupCompanies { (company) -> Bool in
            if let id = selectedFilter.type {
                return company.availableDeliveryTypes?.contains(id) ?? false
            } else {
                return true
            }
        }
    }
    
    func countOfCompanies(by datum: FilterProtocol) -> UInt {
        return companyDataSource.availableDeliveryCompaniesCount(by: datum)
    }
    
    func loadFilters(_ completion: ErrorClosure?) {
        request(.city) { (result) in
            switch result {
            case .success(let value):
                self.items = value.deliveryData ?? []
                completion?(nil)
            case .failure(let error):
                completion?(error)
            }
        }
    }
}

extension DeliveryDatum: FilterProtocol {}
