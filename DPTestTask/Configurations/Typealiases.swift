//
//  Typealiases.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import Foundation

typealias EmptyClosure = () -> ()

typealias GenericClosure<T> = (T) -> ()
typealias GenericErrorClosure<T> = (T?, Error?) -> ()

typealias ErrorClosure = GenericClosure<Error?>
typealias ResultClosure<T> = GenericClosure<Result<T, Error>>

typealias FilterClosure = (CompanyProtocol) -> Bool
