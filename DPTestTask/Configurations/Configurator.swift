//
//  Configurator.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import Foundation
import SwinjectStoryboard

extension SwinjectStoryboard {
    @objc class func setup() {
        defaultContainer.register(DashboardInteractorProtocol.self) { _ in DashboardInteractor.shared }
        defaultContainer.register(DashboardRouterProtocol.self, factory: { _ in DashboardRouter() })
        defaultContainer.register(DashboardPresenterProtocol.self) { r in
            return DashboardPresenter(interactor: r.resolve(DashboardInteractorProtocol.self)!, router: r.resolve(DashboardRouterProtocol.self)!)
        }
        defaultContainer.storyboardInitCompleted(DashboardTableViewController.self) { r, c in
            c.presenter = r.resolve(DashboardPresenterProtocol.self)
        }
        
        defaultContainer.register(FilterInteractorProtocol.self, factory: { r in
            return FilterInteractor(companyDataSource: r.resolve(DashboardInteractorProtocol.self)!)}
        )
        defaultContainer.register(FilterPresenterProtocol.self, factory: { r in
            return FilterPresenter(interactor: r.resolve(FilterInteractorProtocol.self)!)
        })
        defaultContainer.storyboardInitCompleted(FilterTableViewController.self, initCompleted: { r, c in
            c.presenter = r.resolve(FilterPresenterProtocol.self)
        })
    }
}
