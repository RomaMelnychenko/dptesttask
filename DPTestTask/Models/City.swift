// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let city = try? newJSONDecoder().decode(City.self, from: jsonData)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseCity { response in
//     if let city = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - City
struct City: Codable {
    let id: Int?
    let name: String?
    let lang: String?
    let status: Int?
    let genitive: String?
    let phone: String?
    let timezone: String?
    let centerCoordinates: CenterCoordinates?
    let searchRadiuses: String?
    let currency: Currency?
    let zoning: Int?
    let chat: Int?
    let chatTimeOut: Int?
    let noticeMessage: JSONNull?
    let deliveryData: [DeliveryDatum]?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case lang = "lang"
        case status = "status"
        case genitive = "genitive"
        case phone = "phone"
        case timezone = "timezone"
        case centerCoordinates = "centerCoordinates"
        case searchRadiuses = "searchRadiuses"
        case currency = "currency"
        case zoning = "zoning"
        case chat = "chat"
        case chatTimeOut = "chatTimeOut"
        case noticeMessage = "noticeMessage"
        case deliveryData = "deliveryData"
    }
}
