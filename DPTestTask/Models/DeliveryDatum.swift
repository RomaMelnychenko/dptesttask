// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let deliveryDatum = try? newJSONDecoder().decode(DeliveryDatum.self, from: jsonData)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDeliveryDatum { response in
//     if let deliveryDatum = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - DeliveryDatum
struct DeliveryDatum: Codable, Equatable {
    let type: Int?
    let title: String?

    enum CodingKeys: String, CodingKey {
        case type = "type"
        case title = "title"
    }
}
