// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let workingTime = try? newJSONDecoder().decode(WorkingTime.self, from: jsonData)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseWorkingTime { response in
//     if let workingTime = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - WorkingTime
struct WorkingTime: Codable {
    let id: Int?
    let start: String?
    let end: String?
    let status: Int?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case start = "start"
        case end = "end"
        case status = "status"
    }
}
