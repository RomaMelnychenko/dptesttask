// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let currency = try? newJSONDecoder().decode(Currency.self, from: jsonData)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseCurrency { response in
//     if let currency = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - Currency
struct Currency: Codable {
    let token: String?
    let formatted: String?

    enum CodingKeys: String, CodingKey {
        case token = "token"
        case formatted = "formatted"
    }
}
