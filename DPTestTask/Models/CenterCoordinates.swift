// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let centerCoordinates = try? newJSONDecoder().decode(CenterCoordinates.self, from: jsonData)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseCenterCoordinates { response in
//     if let centerCoordinates = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - CenterCoordinates
struct CenterCoordinates: Codable {
    let latitude: String?
    let longitude: String?

    enum CodingKeys: String, CodingKey {
        case latitude = "latitude"
        case longitude = "longitude"
    }
}
