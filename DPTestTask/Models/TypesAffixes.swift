// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let typesAffixes = try? newJSONDecoder().decode(TypesAffixes.self, from: jsonData)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseTypesAffixes { response in
//     if let typesAffixes = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - TypesAffixes
struct TypesAffixes: Codable {
    let types: String?
    let typesGenitive: String?
    let typesDative: String?
    let typesAccusative: String?
    let typesAblative: String?
    let typesPrepositional: String?

    enum CodingKeys: String, CodingKey {
        case types = "types"
        case typesGenitive = "typesGenitive"
        case typesDative = "typesDative"
        case typesAccusative = "typesAccusative"
        case typesAblative = "typesAblative"
        case typesPrepositional = "typesPrepositional"
    }
}
