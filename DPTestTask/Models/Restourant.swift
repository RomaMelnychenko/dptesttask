// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let restourant = try? newJSONDecoder().decode(Restourant.self, from: jsonData)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseRestourant { response in
//     if let restourant = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - Restourant
struct Restourant: Codable {
    let id: Int?
    let name: String?
    let nameGenitive: String?
    let nameDative: String?
    let nameAccusative: String?
    let nameAblative: String?
    let namePrepositional: String?
    let restourantDescription: String?
    let note: String?
    let status: Int?
    let image: String?
    let priority: Int?
    let textOffCompany: String?
    let todayWorkingTimeText: String?
    let workingTime: [WorkingTime]?
    let isWorkingNow: Int?
    let actionText: String?
    let actionInformerText: [JSONAny]?
    let specializedImages: [String]?
    let specializedCategoriesIds: [Int]?
    let recommendedCategoriesIds: [JSONAny]?
    let showReviewsTab: Bool?
    let availableDeliveryTypes: [Int]?
    let type: String?
    let typesAffixes: TypesAffixes?
    let onlinePayment: Int?
    let terminalPayment: Int?
    let rating: Double?
    let reviewsCount: Int?
    let showRating: Bool?
    let deliveryPriceText: String?
    let deliveryPriceInformerText: [String]?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case nameGenitive = "nameGenitive"
        case nameDative = "nameDative"
        case nameAccusative = "nameAccusative"
        case nameAblative = "nameAblative"
        case namePrepositional = "namePrepositional"
        case restourantDescription = "description"
        case note = "note"
        case status = "status"
        case image = "image"
        case priority = "priority"
        case textOffCompany = "textOffCompany"
        case todayWorkingTimeText = "todayWorkingTimeText"
        case workingTime = "workingTime"
        case isWorkingNow = "isWorkingNow"
        case actionText = "actionText"
        case actionInformerText = "actionInformerText"
        case specializedImages = "specializedImages"
        case specializedCategoriesIds = "specializedCategoriesIds"
        case recommendedCategoriesIds = "recommendedCategoriesIds"
        case showReviewsTab = "showReviewsTab"
        case availableDeliveryTypes = "availableDeliveryTypes"
        case type = "type"
        case typesAffixes = "typesAffixes"
        case onlinePayment = "onlinePayment"
        case terminalPayment = "terminalPayment"
        case rating = "rating"
        case reviewsCount = "reviewsCount"
        case showRating = "showRating"
        case deliveryPriceText = "deliveryPriceText"
        case deliveryPriceInformerText = "deliveryPriceInformerText"
    }
}
