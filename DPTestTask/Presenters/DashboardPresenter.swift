//
//  DashboardPresenter.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit

class DashboardPresenter: NSObject {
    
    var interactor: DashboardInteractorProtocol
    var router: DashboardRouterProtocol
    
    init(interactor: DashboardInteractorProtocol, router: DashboardRouterProtocol) {
        self.interactor = interactor
        self.router = router
        
        super.init()
    }
    
    fileprivate var moveToFilter: EmptyClosure?
}

extension DashboardPresenter: DashboardPresenterProtocol {
    
    func load(with view: DashboardViewProtocol) {
        interactor.loadRestaurants { (error) in
            if let error = error {
                view.handle(error: error)
            } else {
                self.moveToFilter = {
                    self.router.moveToFilter(with: view)
                }
                view.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DashboardTableViewCell = tableView.dequeue(for: indexPath)
        cell.company = interactor.companies[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: DashboardHeaderView = tableView.dequeue()
        header.onClick = moveToFilter
        return header
    }
}
