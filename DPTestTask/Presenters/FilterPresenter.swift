//
//  FilterPresenter.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 14.04.2021.
//

import UIKit

class FilterPresenter: NSObject {
    
    fileprivate var selectedFilter: FilterProtocol? {
        didSet {
            guard let filter = selectedFilter else { return }
            let count = interactor.countOfCompanies(by: filter)
            updateCountClosure?(count)
        }
    }
    
    fileprivate var interactor: FilterInteractorProtocol
    fileprivate var updateCountClosure: GenericClosure<UInt>?
    
    init(interactor: FilterInteractorProtocol) {
        self.interactor = interactor
        
        super.init()
    }
}

extension FilterPresenter: FilterPresenterProtocol {
    
    func setupFilter() {
        guard let selectedFilter = selectedFilter else { return }
        interactor.updateFilterCLosure(with: selectedFilter)
    }
    
    func load(with view: FilterViewProtocol) {
        interactor.loadFilters { error in
            if let error = error {
                view.handle(error: error)
            } else {
                self.updateCountClosure = view.update(with:)
                view.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.filters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FilterTableViewCell = tableView.dequeue(for: indexPath)
        let filter = interactor.filters[indexPath.row]
        cell.filter = filter
        
        cell.setSelected(filter.type == selectedFilter?.type, animated: true)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let filter = interactor.filters[indexPath.row]
        selectedFilter = filter
    }
}
