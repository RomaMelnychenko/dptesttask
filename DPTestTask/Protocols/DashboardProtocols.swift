//
//  DashboardProtocols.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit


protocol CompanyProtocol {
    var id: Int? { get }
    var name: String?  { get }
    var nameGenitive: String?  { get }
    var nameDative: String?  { get }
    var nameAccusative: String?  { get }
    var nameAblative: String?  { get }
    var namePrepositional: String?  { get }
    var restourantDescription: String?  { get }
    var status: Int?  { get }
    var image: String?  { get }
    var priority: Int?  { get }
    var textOffCompany: String?  { get }
    var todayWorkingTimeText: String?  { get }
    var schedule: [WorkingTimeProtocol]?  { get }
    var isWorkingNow: Int?  { get }
    var actionText: String?  { get }
    var specializedImages: [String]?  { get }
    var specializedCategoriesIds: [Int]?  { get }
    var showReviewsTab: Bool?  { get }
    var availableDeliveryTypes: [Int]?  { get }
    var type: String?  { get }
    var onlinePayment: Int?  { get }
    var terminalPayment: Int?  { get }
    var rating: Double?  { get }
    var reviewsCount: Int?  { get }
    var showRating: Bool?  { get }
    var deliveryPriceText: String?  { get }
    var deliveryPriceInformerText: [String]?  { get }
    var saleCount: UInt? { get }
}

protocol WorkingTimeProtocol {
    var id: Int? { get }
    var start: String? { get }
    var end: String? { get }
    var status: Int? { get }
}

protocol CompaniesDataSource: class {
    var companies: [CompanyProtocol] { get }
    func availableDeliveryCompaniesCount(by filter: FilterProtocol) -> UInt
    func setupCompanies(filter: FilterClosure?)
}

protocol DashboardInteractorProtocol: CompaniesDataSource {
    func loadRestaurants(_ completion: ErrorClosure?)
}

protocol DashboardPresenterProtocol: UITableViewDataSource, UITableViewDelegate {
    func load(with view: DashboardViewProtocol)
}

protocol DashboardViewProtocol: class {
    var tableView: UITableView! { get }
    func handle(error: Error)
    func performSegue(withIdentifier: String, sender: Any?)
}

protocol DashboardRouterProtocol {
    func moveToFilter(with currentViewController: DashboardViewProtocol)
}
