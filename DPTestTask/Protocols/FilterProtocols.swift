//
//  FilterProtocols.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 14.04.2021.
//

import UIKit

protocol FilterPresenterProtocol: UITableViewDataSource, UITableViewDelegate {
    func load(with view: FilterViewProtocol)
    func setupFilter()
}

protocol FilterInteractorProtocol {
    var filters: [FilterProtocol] { get }
    
    func loadFilters(_ completion: ErrorClosure?)
    func countOfCompanies(by datum: FilterProtocol) -> UInt
    func updateFilterCLosure(with selectedFilter: FilterProtocol)
}

protocol FilterProtocol {
    var type: Int? { get }
    var title: String? { get }
}

protocol FilterViewProtocol: class {
    var tableView: UITableView! { get }
    
    func handle(error: Error)
    func update(with count: UInt)
}
