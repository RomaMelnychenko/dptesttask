//
//  FilterTableViewController.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 14.04.2021.
//

import UIKit

class FilterTableViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var countOfCompaniesLabel: UILabel!
    
    var presenter: FilterPresenterProtocol?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        configureTableView()
        bottomView.isHidden = true
        
        presenter?.load(with: self)
    }
    
    func configureTableView() {
        tableView.register(cell: FilterTableViewCell.self)
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        footerView.backgroundColor = .clear
        tableView.tableFooterView = footerView
        
        tableView.dataSource = presenter
        tableView.delegate = presenter
    }
    
    @IBAction func onApprove(_ sender: UIButton) {
        presenter?.setupFilter()
        navigationController?.popViewController(animated: true)
    }
}

extension FilterTableViewController: FilterViewProtocol {
    
    func handle(error: Error) {
        print(error)
    }
    
    func update(with count: UInt) {
        bottomView.isHidden = count == 0
        countOfCompaniesLabel.text = "\(count) закладів"
    }
}
