//
//  DashboardTableViewController.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit

class DashboardTableViewController: UITableViewController {

    var presenter: DashboardPresenterProtocol?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar()
        configureTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter?.load(with: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let title = "Перемоги проспект 105, Чернігів"
        self.title = title
        navigationItem.title = title
    }
    
    fileprivate func configureNavigationBar() {
        let locationItem = UIBarButtonItem(image: UIImage(named: "Maps-Location-icon"), style: .plain, target: self, action: #selector(onLocationChange))
        locationItem.tintColor = .white
        navigationItem.leftBarButtonItem = locationItem
        
        let trolleyItem = UIBarButtonItem(image: UIImage(named: "shopping-cart"), style: .plain, target: self, action: #selector(onTrolley))
        trolleyItem.tintColor = .white
        navigationItem.rightBarButtonItem = trolleyItem
        
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = .customGreen
    }
    
    fileprivate func configureTableView() {
        tableView.register(cell: DashboardTableViewCell.self)
        tableView.register(header: DashboardHeaderView.self)
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        footerView.backgroundColor = .clear
        tableView.tableFooterView = footerView
        
        tableView.separatorInset = .zero
        
        tableView.dataSource = presenter
        tableView.delegate = presenter
    }
    
    @objc func onLocationChange(_ sender: Any) {
        
    }
    
    @objc func onTrolley(_ sender: Any) {
        
    }

}

extension DashboardTableViewController: DashboardViewProtocol {
    func reload() {
        tableView.reloadData()
    }
    
    func handle(error: Error) {
        print("error - \(error)")
    }
}
