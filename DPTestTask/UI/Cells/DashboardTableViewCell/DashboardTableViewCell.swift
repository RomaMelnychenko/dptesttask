//
//  DashboardTableViewCell.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit
import SDWebImage

class DashboardTableViewCell: UITableViewCell {

    @IBOutlet var restImageView: UIImageView!
    @IBOutlet var restNameLabel: UILabel!
    
    @IBOutlet var markupLabel: UILabel!
    @IBOutlet var saleButton: SaleButton!
    
    @IBOutlet var deliveryLabel: UILabel!
    
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var maxRatingLabel: UILabel!
    @IBOutlet var scheduleLabel: UILabel!
    @IBOutlet var stateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var company: CompanyProtocol? {
        didSet {
            if let path = company?.image {
                restImageView.sd_setImage(with: URL(string: path))
            }
            restNameLabel.text = company?.name?.convertAscii
            
            markupLabel.text = company?.actionText?.convertAscii
            saleButton.count = company?.saleCount ?? 0
            
            if let deliveryText = company?.deliveryPriceText?.convertAscii {
                deliveryLabel.text = "Доставка \(deliveryText)"
            } else {
                deliveryLabel.text = company?.deliveryPriceText?.convertAscii
            }
            
            [ratingLabel, maxRatingLabel].forEach({ $0?.isHidden = !(company?.showRating ?? false) })
            if let rating = company?.rating {
                ratingLabel.text = "\(rating)"
            }
            scheduleLabel.text = company?.todayWorkingTimeText
            switch company?.isWorkingNow {
            case 0:
                stateLabel.text = "закрито"
            case 1:
                stateLabel.text = "відкрито"
            default:
                break
            }
        }
    }
}
