//
//  FilterTableViewCell.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 14.04.2021.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var filterImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        filterImageView.image = UIImage(named: selected ? "selected-filter" : "unselected-filter")
    }
    
    var filter: FilterProtocol? {
        didSet {
            titleLabel.text = filter?.title
        }
    }
}
