//
//  DashboardHeaderView.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit

class DashboardHeaderView: UITableViewHeaderFooterView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    

    var onClick: EmptyClosure? {
        didSet {
            if let _ = onClick {
                let tap = UITapGestureRecognizer(target: self, action: #selector(onTap))
                addGestureRecognizer(tap)
                tapGestureRecognizer = tap
            } else if let tapGestureRecognizer = tapGestureRecognizer {
                removeGestureRecognizer(tapGestureRecognizer)
            }
        }
    }
    
    fileprivate var tapGestureRecognizer: UITapGestureRecognizer?
    
    @objc fileprivate func onTap() {
        onClick?()
    }
}
