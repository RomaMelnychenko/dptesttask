//
//  StatusLabel.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit

class StatusLabel: UILabel {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        font = .roboto(.regular, size: 12)
        textColor = .customGreen
    }

}
