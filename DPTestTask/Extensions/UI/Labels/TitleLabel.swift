//
//  TitleLabel.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit

class TitleLabel: UILabel {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        font = .roboto(.regular, size: 16)
        textColor = .customGreen
    }

}
