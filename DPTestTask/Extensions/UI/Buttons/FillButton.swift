//
//  FillButton.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 15.04.2021.
//

import UIKit

class FillButton: UIButton {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        layer.cornerRadius = frame.height / 2
        layer.masksToBounds = true
    }
}
