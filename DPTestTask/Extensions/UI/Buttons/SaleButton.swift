//
//  SaleButton.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit

class SaleButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel?.font = .roboto(.regular, size: 12)
        titleLabel?.textColor = .customOrange
        setTitleColor(.customOrange, for: .normal)
        
        layer.borderWidth = 1
        layer.borderColor = UIColor.customOrange.cgColor
        
        isHidden = true
        roundCorners()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        roundCorners()
    }
    
    var count: UInt = 0 {
        didSet {
            isHidden = count == 0
            if !isHidden {
                setTitle(.sale(count: count), for: .normal)
            }
        }
    }
    
    func roundCorners() {
        let height = bounds.height
        layer.cornerRadius = height/2
        layer.masksToBounds = true
    }
}
