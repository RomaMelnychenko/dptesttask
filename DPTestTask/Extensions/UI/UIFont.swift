//
//  UIFont.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit

extension UIFont {
    
    enum Style: String {
        case regular
    }
    
    static func roboto(_ style: Style, size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-\(style.rawValue.firstCapitalized)", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
