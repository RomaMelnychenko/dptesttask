//
//  UIColor.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import UIKit

extension UIColor {
    
    static let customGreen = UIColor(red: 0/255, green: 81/255, blue:0/255, alpha: 1.0)
    static let customBlack = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
    static let customGrey  = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
    static let customOrange = UIColor(red: 1, green: 165/255, blue: 0, alpha: 1.0)
}
