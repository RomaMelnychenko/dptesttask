//
//  String.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 13.04.2021.
//

import Foundation

extension String {
    
    static func sale(count: UInt) -> String {
        let secondWord: String
        let value = count < 20 ? count : count%10
        switch value {
        case 1:
            secondWord = "акція"
        case 2...4:
            secondWord = "акції"
        default:
            secondWord = "акцій"
        }
        return "\(count) \(secondWord)"
    }
}

extension String {
    
    var convertAscii: String {
        var source = self
        var result = ""
        while source.count >= 2 {
            let digitsPerCharacter = source.hasPrefix("1") ? 3 : 2
            let charBytes = source.prefix(digitsPerCharacter)
            source = String(source.dropFirst(digitsPerCharacter))
            if let number = Int(charBytes) {
                let character = UnicodeScalar(number)!
                result += String(character)
            } else {
                result += String(charBytes)
            }
        }
        return result
    }
    
    var firstCapitalized: String {
        return prefix(1).capitalized + dropFirst()
    }
}
