//
//  DashboardRouter.swift
//  DPTestTask
//
//  Created by Roma Melnychenko on 15.04.2021.
//

import UIKit

class DashboardRouter: DashboardRouterProtocol {
    
    func moveToFilter(with currentViewController: DashboardViewProtocol) {
        currentViewController.performSegue(withIdentifier: "filterSegue", sender: self)
    }
}
